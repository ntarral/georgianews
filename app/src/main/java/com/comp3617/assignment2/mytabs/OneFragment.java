package com.comp3617.assignment2.mytabs;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;


public class OneFragment extends Fragment{

    private ImageView img;
    private TextView txt;
    private ListView titleTasks;
    private EvtListAdapter adapter;
    private static final String[] lists = {"aquarelle", "dance with the star", "cinema night", "bike in the night"};
    private String[] extLists;

    public OneFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.extLists = getArguments().getStringArray("extLists");
        Log.d("1FRAG", this.extLists[0]);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=  inflater.inflate(R.layout.fragment_one, container, false);



        titleTasks = (ListView) v.findViewById(R.id.tabOne);
        adapter = new EvtListAdapter(getActivity(),new ArrayList<String>(Arrays.asList(extLists)));
        titleTasks.setAdapter(adapter);

        return v;
    }

}
