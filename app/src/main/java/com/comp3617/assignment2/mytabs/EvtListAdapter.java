package com.comp3617.assignment2.mytabs;


import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by THOR on 6/27/2016.
 */
public class EvtListAdapter extends ArrayAdapter<String> {

    private Context ctx;
    private List<String> list;
    private ImageView img;
    private TextView logo;


    public EvtListAdapter(Context context, List<String> list) {
        super(context, 0, list);
        this.ctx = context;
        this.list = list;

    }



    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = null;

        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rowView = inflater.inflate(R.layout.row_layout, parent, false);
        logo = (TextView) rowView.findViewById(R.id.cityText);
        Resources res = null;
        img = (ImageView) rowView.findViewById(R.id.img);
        logo.setText(list.get(position));
        img.setImageResource(R.drawable.cloudy);


        return rowView;

    }


}
